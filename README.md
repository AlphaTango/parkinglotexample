# README - OOP Parking Lot Example #

A simple example of a parking lot, implemented as an OOP system. This is a common interview question on OOP system design.

### How does this work? ###

Run ParkingLotMain as a Java application. this creates a ParkingLot object, with 2 free parking spaces. It then creates 4 cars (two large, two small) and attempts to issues tickets to three of them. It won't be possible to issue a ticket to the third card (as the lot will be full), a message to this effect is printed to the stand output. The main thread then sleeps for a while and then receives a ticket from the second and first cars; the third and fourth car are then able to enter.

### How is this design structured?  ###

ParkingLot Main. java - this is the main class, used to set up and run the system.

ParkingLot.java - this is an object used to represent the parking lot itself. This has two methods, issueTicket(AbstractCar car) and receiveTicket(AbstractCar car) and maintains a map which takes the car's numberplate as a key, and a ParkingTicket object as the value. When a car arrives, if a space is available in the parking lot a new ParkingTicket is created. When a car departs, it first pays for it's ticket and then it's license place is used to retreive the ticket from the map and verify that it's been paid for, if not the ticket is not removed from the list and the driver is informed that they must pay, otherwise the car is allowed to leave and is removed from the map.

ParkingTicket.java - this is an object used to represent a parking ticket. The contructor has two parameters: the time of arrival and the ID number for the ticket. When a car departs, the departure time stamp on the ticket is set and the price of the ticket is calculated by subtracting the departure time from the arrival time and multiplying by the price per minute (which can be changed, e.g.: depending on the type of AbstractCar). Because this object will be stored in a map, it also has the hashCode() and equals() methods overriden.

AbstractCar.java - this is an abstract class which can be used to create different subtypes of cars. The constructor requires a String which will be used as the numberplate, and on stores the ParkingTicket which is provided on arrival at the parking lot. There is also an abstract hornSound() method which must be implemented by subclasses. On departure, the AbstractCar pays for the ticket and then plays the car's horn sound.

SmallCar.java and LargeCar.java - these are concrete implementions of AbstractCar, each with the hornSound method overriden and producing a different sound.


### Why did I use feature X or process Y? ###

I used a ConcurrentHashMap as this would make it easier should the need to use multiple threads arise later (e.g.: adding two entrances to the parking lot) and has the benefit of not locking when being read from and con therefor overlap with update operations (e.g.: put and remove).  
I did also opt to use a volatile int to track the number of free places in the map - I did this on the basis that it's faster than traversing the map each time to see how many elements are in the map and comparing it to the size of the map. Furthermore, the map is automatically resized when the average number of elements per bin exceeds a certain size, which would further complicate calculating the number of free spaces.  
I created an AbstractCar on the basis that it might be desirable to charge different prices for different types of car at some point. An alternative might be to create a Car interface instead and use the default method functionality introduced in Java 8.  
I decided not to create a tickt machine but simply to integrate the functionality it would have provided into the ParkingLot object instead as this seemed more efficient, however this might be one way of extending the system in future.  