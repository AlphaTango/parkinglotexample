package main;


public class ParkingLotMain {

	public ParkingLotMain() {
		// TODO: set size of parking ticket list/number of spaces
	}
	
	public static void main(String[] args) throws InterruptedException {
		// TODO: simulate some cars arriving and departing
		
		ParkingLot parkingLot = new ParkingLot(2);
		
		SmallCar firstCar = new SmallCar("B1GWH33L");
		SmallCar secondCar = new SmallCar("B34ST01");
		LargeCar thirdCar = new LargeCar("V12EURO");
		LargeCar fourthCar = new LargeCar("V8MURIKA");
		
		parkingLot.issueTicket(firstCar);
		parkingLot.issueTicket(secondCar);
		parkingLot.issueTicket(thirdCar);
		
		Thread.sleep(5000);
		parkingLot.receiveTicket(secondCar);
		
		Thread.sleep(2500);
		parkingLot.receiveTicket(firstCar);
		
		parkingLot.issueTicket(thirdCar);
		parkingLot.issueTicket(fourthCar);
		
		Thread.sleep(4000);
		parkingLot.receiveTicket(thirdCar);
		
		Thread.sleep(2000);
		parkingLot.receiveTicket(fourthCar);
	}
	
}
