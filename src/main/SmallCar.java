package main;

public class SmallCar extends AbstractCar {

	public SmallCar(String numberPlate) {
		super(numberPlate);
	}

	@Override
	public void hornSound() {
		System.out.println("Beep Beep!");
	}

}
