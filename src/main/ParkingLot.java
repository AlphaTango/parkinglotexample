package main;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ParkingLot {

	private int currentTicketId = 0;
	private Map<String, ParkingTicket> parkingSpaces;	// we'll be using a concurrentHasMap, which is thread safe
	private volatile int freeSpaces;					// however this needs to be volatile to make sure the number
														// of free spaces is accurate (faster than using map.size())
	
	public ParkingLot(int numberOfSpaces) {
		parkingSpaces = new ConcurrentHashMap<String, ParkingTicket>(numberOfSpaces);
		freeSpaces = numberOfSpaces;
	}
	
	public void issueTicket(AbstractCar car) {
		if (freeSpaces > 0) {
			ParkingTicket ticket = new ParkingTicket(System.currentTimeMillis() / 1000, currentTicketId++);
			parkingSpaces.put(car.getNumberPlate(), ticket);
			freeSpaces--;
			car.arrive(ticket);	
		} else {
			System.out.println("Sorry - the car park is full!");
		}
		
	}
	
	public void receiveTicket(AbstractCar car) {
		car.depart();
		if (parkingSpaces.get(car.getNumberPlate()).isPaid()) {
			System.out.println("Thanks for staying!");
			parkingSpaces.remove(car.getNumberPlate());
			freeSpaces++;
		} else {
			System.out.println("You haven't paid your ticket!");
		}
	
	}
}
