package main;

public class LargeCar extends AbstractCar {

	public LargeCar(String numberPlate) {
		super(numberPlate);
	}

	@Override
	public void hornSound() {
		System.out.println("Honk honk!");
	}

}
