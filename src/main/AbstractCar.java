package main;

public abstract class AbstractCar {
	
	private ParkingTicket ticket;
	private String numberPlate;

	public AbstractCar(String numberPlate) {
		this.numberPlate = numberPlate;
	}
	
	public abstract void hornSound();

	// arrive
	public void arrive(ParkingTicket ticket) {
		this.ticket = ticket;
	}
	
	// depart
	public void depart() {
		ticket.payTicket();
		hornSound();
	}

	
	// setters and getters
	public String getNumberPlate() {
		return numberPlate;
	}

	public void setNumberPlate(String numberPlate) {
		this.numberPlate = numberPlate;
	}
	


}
