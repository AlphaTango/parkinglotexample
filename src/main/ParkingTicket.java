package main;

public class ParkingTicket {
	
	private long arrivalTime;	// time of car's arrival
	private long departureTime;	// time of car's departure
	
	private int ticketId; 
	
	private float costPerMinute = 1.0f;	// cost per minute to park (this is actually cost per second)
	private float totalCost;
	
	private boolean paid = false;
	
	/**
	 * constructor
	 * @param arrivalTime
	 */
	public ParkingTicket(long arrivalTime, int ticketId) {
		this.arrivalTime = arrivalTime;
		this.ticketId = ticketId;
		System.out.println("ticket issued at: " + this.arrivalTime);
	}
	
	public void payTicket() {
		departureTime = System.currentTimeMillis() / 1000;
		totalCost = (departureTime - arrivalTime) * costPerMinute;
		paid = true;
		System.out.println("total cost of ticket ID " + ticketId + ": " + totalCost);
	}
	
	
	// setters and getters / mutators
	public long getArrivalTime() {
		return arrivalTime;
	}

	public void setDepartureTime(long departureTime) {
		this.departureTime = departureTime;
	}

	public void setCostPerMinute(float costPerMinute) {
		this.costPerMinute = costPerMinute;
	}
	
	public long getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (arrivalTime ^ (arrivalTime >>> 32));
		result = prime * result + ticketId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParkingTicket other = (ParkingTicket) obj;
		if (arrivalTime != other.arrivalTime)
			return false;
		if (ticketId != other.ticketId)
			return false;
		return true;
	}	
	
	
	
}
